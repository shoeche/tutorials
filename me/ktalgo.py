
import math as m

from vector import Vec4
from particle import Particle

class KTAlgo:

    def __init__(self,kT,R,y):
        self.kT = kT
        self.R = R
        self.y = y
    
    def Q2ij(self,p,q):
        pt2 = p.PT2()
        qt2 = q.PT2()
        if pt2>0. and qt2>0.:
            D = m.cosh(p.Y()-q.Y())-m.cos(p.Phi()-q.Phi())
            return 2.*min(pt2,qt2)*D/self.R**2
        if pt2>0.: return pt2
        else: return qt2

    def Cluster(self,event):
        q2 = (event[0].mom+event[1].mom).M2()
        p = [ i.mom for i in event ]
        kt2 = []
        jets = []
        n = len(p)
        imap = [ i for i in range(n) ]
        kt2ij = [ [ 0 for i in range(n) ] for j in range(n) ]
        dmin = q2
        for i in range(2,n):
            for j in range(i):
                dij = kt2ij[i][j] = self.Q2ij(p[i],p[j])
                if dij < dmin: dmin = dij; ii = i; jj = j
        while n>2:
            n -= 1
            kt2.append(dmin);
            jjx = imap[jj]
            if jjx>2: p[jjx] += p[imap[ii]]
            else:
                if dmin>pow(self.kT,2) and \
                   abs(p[imap[ii]].Y())<self.y: jets.append(p[imap[ii]])
            for i in range(ii,n): imap[i] = imap[i+1]
            for j in range(jj): kt2ij[jjx][imap[j]] = self.Q2ij(p[jjx],p[imap[j]])
            for i in range(jj+1,n): kt2ij[imap[i]][jjx] = self.Q2ij(p[jjx],p[imap[i]])
            dmin = q2
            for i in range(2,n):
                for j in range(i):
                    dij = kt2ij[imap[i]][imap[j]]
                    if dij < dmin: dmin = dij; ii = i; jj = j
        return jets
