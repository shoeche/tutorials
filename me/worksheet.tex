\documentclass[10pt,fleqn]{scrartcl}

\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{helvet}
\usepackage{url}

\begin{document}

\title{\vspace*{-2em} Introduction to Matrix-Element Generators and Phase-Space Integrators}
\author{Tutorial for summer schools}
\date{}
\maketitle

\section{Introduction}

In this tutorial we will discuss the construction of a matrix-element generator
based on the Berends-Giele recursion, and the construction of a simple, yet efficient
phase-space generator for Large Hadron Collider physics.
At the end, you will be able to run your own parton-level event generator for
$pp\to ng$ at LHC energies and compare its predictions to results from other programs.

\section{Getting started}

You can use any of the docker containers for the school to run this tutorial.
Should you have problems with disk space, consider running
\texttt{docker containers prune} and \texttt{docker system prune} first.
To launch the docker container, use the following command
\begin{verbatim}
  docker run -it -u $(id -u $USER) --rm -v $HOME:$HOME -w $PWD <container name>
\end{verbatim}
You can also use your own PC (In this case you should have PyPy and Rivet installed,
and you will need PyPy bindings for LHAPDF). Download the tutorial and change
to the relevant directory by running
\begin{verbatim}
  git clone https://gitlab.com/shoeche/tutorials.git && cd tutorials/me/
\end{verbatim}
For simplicity, this tutorial uses PyPy, a just-in-time compiled variant of Python.
If you are unfamiliar with Python, think of it as yet another scripting language,
such as bash, but way more powerful. A peculiar feature of Python, and indeed its
biggest weakness, is that code is structured by indentation.
That means you need to pay careful attention to all the spaces in this worksheet.
Missing spaces, or additional ones may render your code entirely useless at best.
The worst case scenario is that it will still run, but produce the wrong answer.

Some important ingredients of QCD calculations have been predefined for you.
This includes four vectors and operations on them, the running
coupling, $\alpha_s$, and a $k_T$ clustering algorithm. All this so you can
fully focus on your matrix-element generator!\\

Get started by creating a file called \texttt{pepper.py}.
We need to import the predefined methods for vectors and spinors,
as well as functions from the math library, which come with the pypy
installation itself.
\begin{verbatim}
import math as m
import cmath as cm

from vector import Vec4
from spinor import Weyl
\end{verbatim}
The basic ingredients of our parton-level event generator are
\begin{itemize}
\item the recursion for partial amplitudes,
\item the helicity and color summation,
\item the phase-space integrator.
\end{itemize}
Let us tackle them one by one.

\section{The Berends-Giele recursion}
We will use the recursive formulation of color-ordered all-gluon helicity amplitudes
introduced in Nucl.\ Phys.\ B306 (1988) 759. It is commonly referred to
as the Berends-Giele (BG) method. If you are unfamiliar with this technique,
ask your tutor about it or read the introduction to BG in hep-ph/9601359.\\

First we define a class that represents a gluon current.
The current can be either external or internal.
\begin{verbatim}
class V_C:
\end{verbatim}
This class will be used to construct the polarization vectors according to
Eq.~(25) in hep-ph/9601359. To reduce the computational effort, we employ
the Weyl-van-der-Waerden spinor formalism, detailed in hep-ph/9805445,
which alows us to write the result in terms of two-component spinors.
First, we need a method that implements the numerator on the
right-hand side of Eq.~(25):
\begin{verbatim}
    def VT(self,a,b):
        e = Vec4(a.u1*b.u1+a.u2*b.u2,0.,0.,0.)
        e[Weyl.r3] = a.u1*b.u1-a.u2*b.u2
        e[Weyl.r1] = a.u1*b.u2+a.u2*b.u1
        e[Weyl.r2] = (a.u1*b.u2-a.u2*b.u1)*1j
        return e
\end{verbatim}
In order to use this function, we need a gauge spinor,
that we construct for both negative and positive helicity
and store as a member of the class.
\begin{verbatim}
    def SetGauge(self,k):
        self.k = k
        self.kp = Weyl(1,self.k)
        self.km = Weyl(-1,self.k)
\end{verbatim}
We are now ready to compute the polarization vectors
for negative and positive helicity:
\begin{verbatim}
    def EM(self,p):
        pp = Weyl(1,p)
        e = self.VT(pp,self.km)
        return e/(m.sqrt(2.)*(self.kp*pp).conjugate())

    def EP(self,p):
        pm = Weyl(-1,p)
        e = self.VT(self.kp,pm)
        return e/(m.sqrt(2.)*(self.km*pm).conjugate())
\end{verbatim}
The next method allows to set the momentum associated with the current,
constructs the correpsonding polarization vector for chirality \texttt{ch},
and stores it in the member variable \texttt{self.j}
\begin{verbatim}
    def ConstructJ(self,p,ch):
        self.p = p
        if ch>=0:
            self.j = self.EP(self.p)
        if ch<=0:
            self.j = self.EM(self.p)
\end{verbatim}
For internal gluon currents, we need to compute propagators. It will be
easiest to use the Feynman gauge in our calculations. The polarization
tensor is then simply a metric tensor, such that for numerical purposes
the propagator takes the form of a scalar propagator:
\begin{verbatim}
    def AddPropagator(self):
        p2 = self.p.M2()
        self.j *= -1j/p2
\end{verbatim}
The next step is to deal with the actual recursion. We define a class
that stores all the necessary currents and handles the computation.
First we store the reference value for the strong coupling:
\begin{verbatim}
class COBG:

    def __init__(self,alphas,n=4):
        self.g3 = m.sqrt(4.*m.pi*alphas(alphas.mz2))
\end{verbatim}
We construct an array of $n(n-1)/2$ currents, which correspond to
the external currents if they live on the diagonal, and to internal
$m$-particle currents with beginning index $b$ and ending index $b+m$
if they are stored in position $(b,b+m)$.
\begin{verbatim}
        self.j = [[0 for i in range(n)] for i in range(n)]
        for b in range(n):
            for e in range(b,n):
                self.j[b][e] = V_C()
\end{verbatim}
We set the gauge vectors of the external currents to a fixed value.
Think: Might there be a better choice? Could we use the gauge vectors
for practical purposes, or are they merely a nuisance?
\begin{verbatim}
        for i in range(len(self.j)):
            self.j[i][i].SetGauge(Vec4(1.,0.,1.,0.))
\end{verbatim}
The two interaction vertices needed in our calculation are the
triple and quartic gluon couplings. We define them using the following functions,
which will be called with arguments that are of class type \texttt{V\_C}:
\begin{verbatim}
    def V3(self,a,b,c):
        jc = (a.j*b.j)*(a.p-b.p)+(a.j*(b.p+b.p+a.p))*b.j-(b.j*(a.p+a.p+b.p))*a.j
        c.j += -self.g3*jc
        c.p = a.p+b.p

    def V4(self,a,e,b,c):
        jc = (e.j*b.j)*a.j+(e.j*a.j)*b.j-2.*(a.j*b.j)*e.j
        c.j += 1j*self.g3**2*jc
\end{verbatim}
Think: In the function for the triple gluon vertex, we set the momentum of the
internal current. Why don't we do so in the function for the quartic coupling?\\

Now we construct the actual BG algorithm. We define an operator that allows
our class to be called as a function, which takes an array of $nn$ gluons
as an input:
\begin{verbatim}
    def __call__(self,event):
        nn = len(self.j)
\end{verbatim}
First we construct polarization vectors for these gluons
\begin{verbatim}
        for i in range(nn):
            self.j[i][i].ConstructJ(event[i].mom,event[i].pol)
\end{verbatim}
Next we iterate over the internal currents, starting with the 2-particle
currents, then moving to 3-particle currents, until we arrive at the $nn-1$-particle
current
\begin{verbatim}
        for n in range(2,nn):
\end{verbatim}
Since we compute color ordered amplitudes, the external particle indices are
consecutive. This means that each $n$-particle current is uniquely specified
by a starting position and an ending position in terms of \textit{external}
particle indices. The starting position can range between 0 and $nn-n-1$.
Think: Why is the range not from 0 to $nn-n$?
\begin{verbatim}
            for b in range(nn-n):
\end{verbatim}
We first reset the current
\begin{verbatim}
                self.j[b][b+n-1].j = Vec4()
\end{verbatim}
Now we construct the internal current by joining currents of lower
multiplicity. To this end, we partition the $n$ external indices into
two ordered subsets of $m$ and $n-m$ indices
\begin{verbatim}
                for m in range(b,b+n-1):
\end{verbatim}
We compute the contribution from the triple gluon vertex to the current,
using the function defined earlier.
\begin{verbatim}
                    self.V3(self.j[b][m],self.j[m+1][b+n-1],self.j[b][b+n-1])
\end{verbatim}
We also need to take into account the quartic gluon coupling. This requires
us to partition the set of $n$ external particle indices into three ordered
subsets
\begin{verbatim}
                    for l in range(m+1,b+n-1):
\end{verbatim}
For the actual computation, we use the method defined earlier
\begin{verbatim}
                        self.V4(self.j[b][m],self.j[m+1][l],self.j[l+1][b+n-1],self.j[b][b+n-1])
\end{verbatim}
If the current corresponds to an off-shell particle, we add the propagator term.
\begin{verbatim}
                if n<nn-1:
                    self.j[b][b+n-1].AddPropagator()
\end{verbatim}
Finally, we contract the on-shell projection of the $n-1$-particle current
and the polarization vector of the last gluon to obtain the complete helicity
amplitude. We return this value for further processing.
\begin{verbatim}
        A = self.j[nn-1][nn-1].j*self.j[0][nn-2].j
        return A
\end{verbatim}

\section{Assembling the squared matrix element}
Computing the color-ordered helicity amplitudes by means of the Berends-Giele
recursion was exciting, but in fact it is just the beginning of our adventures.
We still need to assemble those amplitudes into a differential cross section.
A few components needed for the calculation have been predefined, such as a $k_T$
algorithm which we will use to implement cuts on the final-state jets, and an
interface to the LHAPDF library. Let us begin by importing some of those:
\begin{verbatim}
import random as r
import lhapdf.lib as lhapdf

from tchannel import TChannel
from ktalgo import KTAlgo
from particle import Particle
\end{verbatim}
We will define the phase-space integrator class, called \texttt{TChannel}, later.
For now we simply assume that it has a method called \texttt{GeneratePoint},
which returns the momentum configuration and an associated weight.
Let us deal with the matrix element assembly first.
We need some boilerplate code:
\begin{verbatim}
class ggtogg:

    def __init__(self,alphas,ecms,kT,R,y):
        self.alphas = alphas
        self.ecms = ecms
        self.cobg = COBG(alphas,4)
        self.tchannel = TChannel(2,2,ecms,kT,y)
        self.kt = KTAlgo(kT,R,y)
\end{verbatim}
The following function will compute the actual squared matrix element.
We begin by resetting the result.
\begin{verbatim}
    def ME2(self,ei):
        me2 = 0
\end{verbatim}
We will use the adjoint representation for the  color decomposition
of the full matrix element. In the four-gluon case, this requires us
to compute two permutations of the final-state gluons,
while holding the initial-state gluons fixed.
\begin{verbatim}
        perms = [[2,3],[3,2]]
\end{verbatim}
We need the associated color matrix for the squared amplitude.
Have a go at computing it. If pen on paper becomes too tedious,
you can use the Form input in subdirectory \texttt{ca/}.
Here is the result:
\begin{verbatim}
        colfs = [[72,36],[36,72]]
\end{verbatim}
There are three independent helicity amplitudes, which cannot be inferred
from symmetry. Let us define their helicities, using $+1$ for positive,
and $-1$ for negative:
\begin{verbatim}
        for pols in [[1,1,-1,-1],[1,-1,-1,1],[1,-1,1,-1]]:
\end{verbatim}
Can you explain why these are the only ones needed for our computation?\\

Now comes the actual calculation. We first create a storage for the amplitudes
corresponding to a single helicity configuration. This will be populated
with values for different gluon permutations.
\begin{verbatim}
            amps = []
\end{verbatim}
We iterate over those permutations, and set up the particle momenta
and polarization indices for the call to the BG recursion
\begin{verbatim}
            for i, perm in enumerate(perms):
                e = [ei[0],ei[1],Particle(21,Vec4(),0),Particle(21,Vec4(),0)]
                e[0].pol = pols[0]
                e[1].pol = pols[1]
                for k in range(len(perm)):
                    e[2+k] = ei[perm[k]]
                    e[2+k].pol = pols[perm[k]]
\end{verbatim}
We call the recursion and store the result
\begin{verbatim}
                amps.append(self.cobg(e))
\end{verbatim}
Once the amplitudes have been computed for all permutations, we multiply
by the associated color factors and sum up the result.
\begin{verbatim}
            for i in range(len(perms)):
                for j in range(len(perms)):
                    me2 += colfs[i][j]*(amps[i]*amps[j].conjugate()).real
\end{verbatim}
Gluons are spin-one particles with eight possible color states. So we need an
initial-state symmetry factor of $1/(2\times 8)^2$. An additional factor
of $1/2!$ is due to the final-state symmetry. Finally, we realize that we
have taken a shortcut and only computed amplitudes where the first gluon
has positive helicity. So we need to multiply by two. This gives the complete
matrix element squared.
\begin{verbatim}
        me2 *= 2 / ((2*8)**2 * 2.)
        return me2
\end{verbatim}
The last step is to combine the partonic matrix element with the phase-space
integration and the partonic flux. We define a new method for that
\begin{verbatim}
    def GeneratePoint(self):
\end{verbatim}
In order to compute the cross section, we will need to track the number of
Monte-Carlo points. We count them using the variable \texttt{nt}
\begin{verbatim}
        nt = 0
\end{verbatim}
Since our phase-space integrator will likely not have 100\% cut efficiency,
and it may generate points outside the allowed phase-space region, we first
check that the points are valid and in the integration range.
If a point is discarded, we increase the counter and keep trying
\begin{verbatim}
        while True:
            nt += 1
            (e, psw) = self.tchannel.GeneratePoint()
            if psw>0. and len(self.kt.Cluster(e))==2: break
\end{verbatim}
Once a point is accepted, we compute the matrix element, and multiply
by phase-space weight and a conversion factor (We like our results
in pb instead of 1/GeV$^2$)
\begin{verbatim}
        me2 = self.ME2(e)
        dxs = me2*psw*3.89379656e8
\end{verbatim}
We use a factorization scale of $m_Z^2$ and compute the PDFs.
Finally, we multiply by the partonic flux and return the result.
\begin{verbatim}
        Q2 = 91.1876**2
        x1 = -e[0].mom.PPlus()/self.ecms
        x2 = -e[1].mom.PMinus()/self.ecms
        dxs *= lhapdf.xPDF(21,x1,Q2)/x1*lhapdf.xPDF(21,x2,Q2)/x2
        dxs /= 2.*x1*x2*pow(self.ecms,2)
        return ( e, dxs, nt )
\end{verbatim}
Let us assemble a few lines to run this code
\begin{verbatim}
if __name__== "__main__":

    import sys, os
    from qcd import AlphaS

    lhapdf.LoadPDF("CT18NLO".encode('ascii'),0)
    alphas = AlphaS(91.1876,lhapdf.alphaS(91.1876**2))
    hardxs = ggtogg(alphas,ecms=13000,kT=30.,R=0.4,y=5.)

    r.seed(123456)
    oint = 1
    sum, sum2, n = 0., 0., 0.
    for i in range(1000000+1):
        e, w, nt = hardxs.GeneratePoint()
        sum += w
        sum2 += w*w
        n += nt
        if i > 0 and i % oint == 0:
            if i / oint == 10: oint *= 10
            tsum = sum/n
            tsum2 = m.sqrt((sum2/n-tsum**2)/(n-1))
            sys.stdout.write('\rEvent {0}: {1:10.4e} pb +- {2:10.4e} pb'.format(i,tsum,tsum2))
            sys.stdout.flush()
    print("")
\end{verbatim}
That's it! You have just created your very first parton-level
event generator. However, before you can run it, we will need
to construct a phase-space integrator as well.

\section{The phase-space generator}
We will use a phase-space generator based on arXiv:2302.10449.
Create a file called \texttt{tchannel.py} and start with the usual
import statements
\begin{verbatim}
import math as m
import random as r

from vector import Vec4
from particle import Particle
\end{verbatim}
Let's define the class and create a constructor that stores the basic numbers:
Number of particles, center-of-mass energy of the collider,
minimum transverse momentum of the jets, and maximum rapidity:
\begin{verbatim}
class TChannel:

    def __init__(self,nin,nout,ecms,ptmin,ymax):
        self.nout = nout
        self.ecms = ecms
        self.pt2min = ptmin**2
        self.ymax = ymax
        self.alpha = 2.0
\end{verbatim}
To generate the transverse momentum spectra, it is useful to understand that
the leading behavior of the matrix element in terms of the transverse momentum
of the final-state gluons is $dp_\perp^2/p_\perp^2$. So we'd like to have the
option to generate $p_\perp^2$ according to a steeply falling distribution.
Let us generalize this to a function of the form $(a+k x)^\alpha$, according
to which we produce events in the interval $[x_-,x_+]$. Convince yourself
that the following Python routine does precisely that.
\begin{verbatim}
    def PeakedDist(self,a,cn,xm,xp,k,ran):
        ce = 1.-cn
        if ce != 0.:
            amin, amax = pow(a+k*xm,ce), pow(a+k*xp,ce)
            x = k*(pow(amin+ran*(amax-amin),1/ce)-a)
            w = (amax-amin)/(k*ce*pow(a+k*x,-cn))
            return (x, w)
        amin, amax = a+k*xm, a+k*xp
        x = k*(amin*pow(amax/amin,ran)-a)
        w = m.log(amax/amin)/(k*pow(a+k*x,-cn))
        return (x, w)
\end{verbatim}
Let's proceed to the actual integration algorithm.
We add some boilerplate function definition
\begin{verbatim}
    def GeneratePoint(self,e=[]):
        if len(e)==0:
            e = [ Particle(21,Vec4(),0) for i in range(2+self.nout) ]
        rtS, S = self.ecms, self.ecms**2
        pt2max = S/4
\end{verbatim}
Now we use Eq.~(5) from arXiv:2302.10449. The first part is the overall weight
\begin{verbatim}
        weight = 2*m.pi/S
\end{verbatim}
Since we have to implement a four-dimensional $\delta$-function eventually,
we must track the total momentum generated up to this point.
\begin{verbatim}
        psum = Vec4(0.,0.,0.,0.)
\end{verbatim}
Next comes the product over all but the last momentum.
\begin{verbatim}
        for i in range(3,2+self.nout):
\end{verbatim}
Each particle has three degrees of freedom, hence we need three random numbers:
\begin{verbatim}
            rn = [ r.random() for k in range(3) ]
\end{verbatim}
We use the function defined earlier to generate the transverse momentum squared.
\begin{verbatim}
            (pt2, wpt2) = self.PeakedDist(0.,self.alpha,self.pt2min,pt2max,1,rn[0])
\end{verbatim}
We multiply by the constant weight factors from Eq.~(5), and by the weight
for the $p_\perp^2$ integral.
\begin{verbatim}
            weight *= wpt2/(32.*pow(m.pi,3))
\end{verbatim}
Next we compute the boundaries of the rapidity integration.
\begin{verbatim}
            yb = m.log(m.sqrt(S/4/pt2)+m.sqrt(S/4/pt2-1.))
            ymax, ymin = min(self.ymax,yb), max(-self.ymax,-yb)
\end{verbatim}
We generate a rapidity and an azimuthal angle and multiply by the associated weights.
\begin{verbatim}
            y = ymin+rn[1]*(ymax-ymin)
            phi = 2*m.pi*rn[2]
            weight *= (ymax-ymin)*2.*m.pi
\end{verbatim}
We construct the momentum and add it to the sum of momenta.
\begin{verbatim}
            sinhy, coshy = m.sinh(y), m.cosh(y)
            e[i].mom = m.sqrt(pt2)*Vec4(coshy,m.cos(phi),m.sin(phi),sinhy)
            psum += e[i].mom
\end{verbatim}
Now we construct the final momentum. It's transverse component is given
by the inverse of the transverse components of the sum previously computed.
The longitudinal component requires the final rapidity integral.
Let us compute the boundaries
\begin{verbatim}
        mj2, ptj2, yj = psum.M2(), psum.PT2(), psum.Y()
        Qt, mt = m.sqrt(ptj2), m.sqrt(mj2+ptj2)
        ymin = rtS/Qt*(1.-mt/rtS*m.exp(-yj))
        ymax = rtS/Qt*(1.-mt/rtS*m.exp(yj))
        if (not ymin>0.) or (not ymax>0.): return (e, 0.)
        ymin, ymax = -m.log(ymin), m.log(ymax)
\end{verbatim}
We carry out the integral and multiply by the associated weight.
\begin{verbatim}
        rn=r.random()
        yv = ymin+rn*(ymax-ymin)
        weight *= ymax-ymin
\end{verbatim}
We construct the last momentum and determine the initial-state momenta
from momentum conservation.
\begin{verbatim}
        sinhy, coshy = m.sinh(yv), m.cosh(yv)
        e[2].mom = Vec4(Qt*coshy,-psum[1],-psum[2],Qt*sinhy)
        pp, pm = (e[2].mom+psum).PPlus(), (e[2].mom+psum).PMinus()
        e[0].mom = -Vec4(pp/2,0,0,pp/2)
        e[1].mom = -Vec4(pm/2,0,0,-pm/2)
\end{verbatim}
It is possible that we have violated overall energy conservation,
so we check for this and happily return if all is well.
\begin{verbatim}
        if not (pp>0. and pp<=rtS and pm>0. and pm<=rtS): weight = 0.
        return (e, weight)
\end{verbatim}


\section{Running the code and things to try next}
Now that you have a shiny new matrix-element generator \textit{and}
phase-space integrator, you can give things a try and run the code
\begin{verbatim}
pypy3 pepper.py
\end{verbatim}
You may want to check that the cross section matches what you expect.\\

There are quite a few directions that can be explored from here.
\begin{itemize}
\item Running couplings and varying renormalization scales.
\item Higher multiplicities. How would you have to extend
  the generator to cover $gg\to 3g$, $gg\to 4g$, ..., $gg\to ng$
  scattering? Want to give the three-gluon case a try?
\item Improving the integration performance: Which algorithms
  might be suitable to achieve faster convergence? Can you use
  any of the knowledge gained in other tutorials?
\item Quark processes. What would have to be done in order to
  make this a full-fledged generator for $pp\to jj$? What
  differences would you expect compared to the gluon-only case?
\item Combination with a parton shower for particle-level
  event generation.
\end{itemize}
You may want to discuss some of these options with your tutor.

\end{document}
