
import math as m

from vector import Vec4

class Particle:

    def __init__(self,pdgid,momentum,pol):
        self.Set(pdgid,momentum,pol)

    def __repr__(self):
        return "{0} {1} {2}".format(self.pid,self.mom,self.pol)

    def __str__(self):
        return "{0} {1} {2}".format(self.pid,self.mom,self.pol)

    def Set(self,pdgid,momentum,pol=0):
        self.pid = pdgid
        self.mom = momentum
        self.pol = pol

def CheckEvent(event):
    psum = Vec4()
    for p in event:
        psum += p.mom
    return (m.fabs(psum.E)<1.e-12 and \
            m.fabs(psum.px)<1.e-12 and \
            m.fabs(psum.py)<1.e-12 and \
            m.fabs(psum.pz)<1.e-12)
