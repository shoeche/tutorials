#procedure colorbar(cf,ct,nc)
.sort
c `ct', `cf'; s `nc';
i ctmpa, ctmpf1, ctmpf2;
id `ct'(ctmpa?,ctmpf1?,ctmpf2?) = `ct'(ctmpa,ctmpf2,ctmpf1);
.sort
#endprocedure

#procedure colortrace(cf,ct,nc)
.sort
c `ct', `cf', ctmpf(s); s `nc', ctcf;
i coli1, ..., coli3, colf1, ..., colf4, cola1, ..., cola3;
#do j = 1,1
  id once   `cf'(cola1?,cola2?,cola3?)
          = 2*i_*`ct'(cola1,coli1,coli2)*
            ( `ct'(cola2,coli2,coli3)*`ct'(cola3,coli3,coli1)
             -`ct'(cola3,coli2,coli3)*`ct'(cola2,coli3,coli1));
  sum coli1, coli2, coli3;
  id once   `cf'(cola1?,cola2?,cola3?)*`ct'(cola3?,colf1?,colf2?)
          = i_*( `ct'(cola1,colf1,coli1)*`ct'(cola2,coli1,colf2)
                -`ct'(cola2,colf1,coli2)*`ct'(cola1,coli2,colf2));
  sum coli1, coli2;
  id once   `cf'(cola1?,cola2?,cola3?)*`ct'(cola2?,colf1?,colf2?)
          = i_*( `ct'(cola3,colf1,coli1)*`ct'(cola1,coli1,colf2)
                -`ct'(cola1,colf1,coli2)*`ct'(cola3,coli2,colf2));
  sum coli1, coli2;
  id once   `cf'(cola1?,cola2?,cola3?)*`ct'(cola1?,colf1?,colf2?)
          = i_*( `ct'(cola2,colf1,coli1)*`ct'(cola3,coli1,colf2)
                -`ct'(cola3,colf1,coli2)*`ct'(cola2,coli2,colf2));
  sum coli1; sum coli2;
  id   `ct'(cola1?,colf1?,colf2?)*`ct'(cola1?,colf2?,colf3?)
     = 1/2*ctcf*ctmpf(colf1,colf3);
  id   `ct'(cola1?,colf1?,colf2?)*`ct'(cola1?,colf3?,colf1?)
     = 1/2*ctcf*ctmpf(colf2,colf3);
  id   `ct'(cola1?,colf1?,colf2?)*`ct'(cola1?,colf3?,colf4?)
     = 1/2*( ctmpf(colf1,colf4)*ctmpf(colf2,colf3)
            -1/`nc'*ctmpf(colf1,colf2)*ctmpf(colf3,colf4));
  id ctmpf(colf1?,colf3?)*ctmpf(colf3?,colf2?) = ctmpf(colf1,colf2);
  id ctmpf(colf1?,colf3?)*ctmpf(colf2?,colf3?) = ctmpf(colf1,colf2);
  id ctmpf(colf3?,colf1?)*ctmpf(colf3?,colf2?) = ctmpf(colf1,colf2);
  id ctmpf(colf3?,colf1?)*ctmpf(colf2?,colf3?) = ctmpf(colf1,colf2);
  id ctmpf(colf1?,colf1?) = `nc';
  if ( count(`cf',1)>0 ) redefine j "0";
  .sort
#enddo
id ctcf = `nc'-1/`nc';
.sort
#endprocedure
