#include color.h

off statistics;

c F(c);
i a1, ..., a101, b1, ..., b101;

l M11 =  F(a2,a1,b1)*F(a3,b1,a4)
        *F(a2,a1,b2)*F(a3,b2,a4);
l M12 =  F(a3,a1,b1)*F(a2,b1,a4)
        *F(a2,a1,b2)*F(a3,b2,a4);

#call colortrace(F,T,NC)

print;
.end
