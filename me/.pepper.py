
import math as m
import cmath as cm

from vector import Vec4
from spinor import Weyl

class V_C:

    def VT(self,a,b):
        e = Vec4(a.u1*b.u1+a.u2*b.u2,0.,0.,0.)
        e[Weyl.r3] = a.u1*b.u1-a.u2*b.u2
        e[Weyl.r1] = a.u1*b.u2+a.u2*b.u1
        e[Weyl.r2] = (a.u1*b.u2-a.u2*b.u1)*1j
        return e

    def SetGauge(self,k):
        self.k = k
        self.kp = Weyl(1,self.k)
        self.km = Weyl(-1,self.k)

    def EM(self,p):
        pp = Weyl(1,p)
        e = self.VT(pp,self.km)
        return e/(m.sqrt(2.)*(self.kp*pp).conjugate())

    def EP(self,p):
        pm = Weyl(-1,p)
        e = self.VT(self.kp,pm)
        return e/(m.sqrt(2.)*(self.km*pm).conjugate())

    def ConstructJ(self,p,ch):
        self.p = p
        if ch>=0:
            self.j = self.EP(self.p)
        if ch<=0:
            self.j = self.EM(self.p)

    def AddPropagator(self):
        p2 = self.p.M2()
        self.j *= -1j/p2

class COBG:

    def __init__(self,alphas,n=4):
        self.g3 = m.sqrt(4.*m.pi*alphas(alphas.mz2))
        self.j = [[0 for i in range(n)] for i in range(n)]
        for b in range(n):
            for e in range(b,n):
                self.j[b][e] = V_C()
        for i in range(len(self.j)):
            self.j[i][i].SetGauge(Vec4(1.,0.,1.,0.))

    def V3(self,a,b,c):
        jc = (a.j*b.j)*(a.p-b.p)+(a.j*(b.p+b.p+a.p))*b.j-(b.j*(a.p+a.p+b.p))*a.j
        c.j += -self.g3*jc
        c.p = a.p+b.p

    def V4(self,a,e,b,c):
        jc = (e.j*b.j)*a.j+(e.j*a.j)*b.j-2.*(a.j*b.j)*e.j
        c.j += 1j*self.g3**2*jc

    def __call__(self,event):
        nn = len(self.j)
        for i in range(nn):
            self.j[i][i].ConstructJ(event[i].mom,event[i].pol)
        for n in range(2,nn):
            for b in range(nn-n):
                self.j[b][b+n-1].j = Vec4()
                for m in range(b,b+n-1):
                    self.V3(self.j[b][m],self.j[m+1][b+n-1],self.j[b][b+n-1])
                    for l in range(m+1,b+n-1):
                        self.V4(self.j[b][m],self.j[m+1][l],self.j[l+1][b+n-1],self.j[b][b+n-1])
                if n<nn-1:
                    self.j[b][b+n-1].AddPropagator()
        A = self.j[nn-1][nn-1].j*self.j[0][nn-2].j
        return A


import random as r

from tchannel import TChannel
from ktalgo import KTAlgo
from particle import Particle

class ggtogg:

    def __init__(self,alphas,ecms,kT,R,y):
        self.alphas = alphas
        self.ecms = ecms
        self.cobg = COBG(alphas,4)
        self.tchannel = TChannel(2,2,ecms,kT,y)
        self.kt = KTAlgo(kT,R,y)

    def ME2(self,ei):
        me2 = 0
        perms = [[2,3],[3,2]]
        colfs = [[72,36],[36,72]]
        for pols in [[1,1,-1,-1],[1,-1,-1,1],[1,-1,1,-1]]:
            amps = []
            for i, perm in enumerate(perms):
                e = [ei[0],ei[1],Particle(21,Vec4(),0),Particle(21,Vec4(),0)]
                e[0].pol = pols[0]
                e[1].pol = pols[1]
                for k in range(len(perm)):
                    e[2+k] = ei[perm[k]]
                    e[2+k].pol = pols[perm[k]]
                amps.append(self.cobg(e))
            for i in range(len(perms)):
                for j in range(len(perms)):
                    me2 += colfs[i][j]*(amps[i]*amps[j].conjugate()).real
        me2 *= 2 / (4*8**2 * 2.)
        return me2

    def GeneratePoint(self):
        nt = 0
        while True:
            nt += 1
            (e, psw) = self.tchannel.GeneratePoint()
            if psw>0. and len(self.kt.Cluster(e))==2: break
        me2 = self.ME2(e)
        dxs = me2*psw*3.89379656e8
        Q2 = 91.1876**2
        x1 = -e[0].mom.PPlus()/self.ecms
        x2 = -e[1].mom.PMinus()/self.ecms
        dxs *= lhapdf.xPDF(21,x1,Q2)/x1*lhapdf.xPDF(21,x2,Q2)/x2
        dxs /= 2.*x1*x2*pow(self.ecms,2)
        return ( e, dxs, nt )

if __name__== "__main__":

    import sys, os
    from qcd import AlphaS
    import lhapdf.lib as lhapdf

    lhapdf.LoadPDF("CT18NLO".encode('ascii'),0)
    alphas = AlphaS(91.1876,lhapdf.alphaS(91.1876**2))
    hardxs = ggtogg(alphas,ecms=13000,kT=30.,R=0.4,y=5.)

    r.seed(123456)
    oint = 1
    sum, sum2, n = 0., 0., 0.
    for i in range(1000000+1):
        e, w, nt = hardxs.GeneratePoint()
        sum += w
        sum2 += w*w
        n += nt
        if i > 0 and i % oint == 0:
            if i / oint == 10: oint *= 10
            tsum = sum/n
            tsum2 = m.sqrt((sum2/n-tsum**2)/(n-1))
            sys.stdout.write('\rEvent {0}: {1:10.4e} pb +- {2:10.4e} pb'.format(i,tsum,tsum2))
            sys.stdout.flush()
    print("")
