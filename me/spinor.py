
import math as m
import cmath as cm
from vector import Vec4

class Weyl:

    r1 = 2
    r2 = 3
    r3 = 1

    accu = 1.e-12

    def SetGauge(self,gauge):
        if gauge == 0:
            Weyl.r1 = 1
            Weyl.r2 = 2
            Weyl.r3 = 3
            return
        if gauge == 1:
            Weyl.r1 = 2
            Weyl.r2 = 3
            Weyl.r3 = 1
            return
        if gauge == 2:
            Weyl.r1 = 3
            Weyl.r2 = 1
            Weyl.r3 = 2
            return
        raise Exception('Weyl')

    def GetK0(self):
        k0 = Vec4(1.,0.,0.,0.)
        k0[Weyl.r3] = -1.
        return k0

    def GetK1(self):
        k1 = Vec4(0.,0.,0.,0.)
        k1[Weyl.r1] = 1.
        return k1

    def PPlus(self,p):
        return p[0]+p[Weyl.r3]

    def PMinus(self,p):
        return p[0]-p[Weyl.r3]

    def PT(self,p):
        return p[Weyl.r1]+p[Weyl.r2]*1j

    def PTC(self,p):
        return p[Weyl.r1]-p[Weyl.r2]*1j

    def __init__(self,r=1,p=0.,q=0.):
        self.r = r
        if not isinstance(p,Vec4):
            self.u1 = p
            self.u2 = q
            return
        pp = self.PPlus(p)
        pm = self.PMinus(p)
        self.u1 = cm.sqrt(pp)
        self.u2 = cm.sqrt(pm)
        pt = self.PT(p)
        sv = abs(p[0])*self.accu
        if (abs(pt.real)>sv or abs(pt.imag)>sv) and \
           (abs(self.u1.real)>sv or abs(self.u1.imag)>sv):
            self.u2 = pt.real + (pt.imag if self.r>0 else -pt.imag)*1j
            self.u2 /= self.u1
        if pp<0. or pm<0.:
            if self.r<0:
                self.u1 = -self.u1.imag + self.u1.real*1j
                self.u2 = -self.u2.imag + self.u2.real*1j
            else:
                self.u1 = self.u1.imag - self.u1.real*1j
                self.u2 = self.u2.imag - self.u2.real*1j

    def __getitem__(self,i):
        if i == 0: return self.u1
        if i == 1: return self.u2
        raise Exception('Weyl')

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '|{0},{1}{2}'.format(self.u1,self.u2,'>' if self.r > 0 else ']')

    def __add__(self,s):
        return Weyl(self.r,self.u1+s.u1,self.u2+s.u2)

    def __sub__(self,s):
        return Weyl(self.r,self.u1-s.u1,self.u2-s.u2)

    def __neg__(self):
        return Weyl(self.r,-self.u1,-self.u2)

    def __mul__(self,s):
        if isinstance(s,Weyl):
            return self.u1*s.u2-self.u2*s.u1 
        return Weyl(self.r,self.u1*s,self.u2*s)

    def __rmul__(self,v):
        if isinstance(s,Weyl):
            return self.u2*s.u1-self.u1*s.u2 
        return Weyl(self.r,self.u1*s,self.u2*s)

    def __div__(self,s):
        return Weyl(self.r,self.u1/s,self.u2/s)
