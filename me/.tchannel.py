
import math as m
import random as r

from vector import Vec4
from particle import Particle

class TChannel:

    def __init__(self,nin,nout,ecms,ptmin,ymax):
        self.nout = nout
        self.ecms = ecms
        self.pt2min = ptmin**2
        self.ymax = ymax
        self.alpha = 2.0

    def PeakedDist(self,a,cn,xm,xp,k,ran):
        ce = 1.-cn
        if ce != 0.:
            amin, amax = pow(a+k*xm,ce), pow(a+k*xp,ce)
            x = k*(pow(amin+ran*(amax-amin),1/ce)-a)
            w = (amax-amin)/(k*ce*pow(a+k*x,-cn))
            return (x, w)
        amin, amax = a+k*xm, a+k*xp
        x = k*(amin*pow(amax/amin,ran)-a)
        w = m.log(amax/amin)/(k*pow(a+k*x,-cn))
        return (x, w)

    def GeneratePoint(self,e=[]):
        if len(e)==0:
            e = [ Particle(21,Vec4(),0) for i in range(2+self.nout) ]
        rtS, S = self.ecms, self.ecms**2
        pt2max = S/4
        weight = 2*m.pi/S
        psum = Vec4(0.,0.,0.,0.)
        for i in range(3,2+self.nout):
            rn = [ r.random() for k in range(3) ]
            (pt2, wpt2) = self.PeakedDist(0.,self.alpha,self.pt2min,pt2max,1,rn[0])
            weight *= wpt2/(32.*pow(m.pi,3))
            yb = m.log(m.sqrt(S/4/pt2)+m.sqrt(S/4/pt2-1.))
            ymax, ymin = min(self.ymax,yb), max(-self.ymax,-yb)
            y = ymin+rn[1]*(ymax-ymin)
            phi = 2*m.pi*rn[2]
            weight *= (ymax-ymin)*2.*m.pi
            sinhy, coshy = m.sinh(y), m.cosh(y)
            e[i].mom = m.sqrt(pt2)*Vec4(coshy,m.cos(phi),m.sin(phi),sinhy)
            psum += e[i].mom
        mj2, ptj2, yj = psum.M2(), psum.PT2(), psum.Y()
        Qt, mt = m.sqrt(ptj2), m.sqrt(mj2+ptj2)
        ymin = rtS/Qt*(1.-mt/rtS*m.exp(-yj))
        ymax = rtS/Qt*(1.-mt/rtS*m.exp(yj))
        if (not ymin>0.) or (not ymax>0.): return (e, 0.)
        ymin, ymax = -m.log(ymin), m.log(ymax)
        rn=r.random()
        yv = ymin+rn*(ymax-ymin)
        weight *= ymax-ymin
        sinhy, coshy = m.sinh(yv), m.cosh(yv)
        e[2].mom = Vec4(Qt*coshy,-psum[1],-psum[2],Qt*sinhy)
        pp, pm = (e[2].mom+psum).PPlus(), (e[2].mom+psum).PMinus()
        e[0].mom = -Vec4(pp/2,0,0,pp/2)
        e[1].mom = -Vec4(pm/2,0,0,-pm/2)
        if not (pp>0. and pp<=rtS and pm>0. and pm<=rtS): weight = 0.
        return (e, weight)

if __name__== "__main__":
    # generate test point

    from particle import CheckEvent

    tchannel = TChannel(2,3,13000,30,4)

    r.seed(123456)
    for i in range(10000):
        (e, w) = tchannel.GeneratePoint()
        if w>0. and not CheckEvent(e):
            sum = Vec4()
            for p in e: sum += p.mom
            print('Event',i,'imbalance',sum)
