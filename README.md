# MC tutorials at the 2024 TASI school

## Getting started

This tutorial uses a container. Please install [Docker](http://docker.com) on your personal computer prior to the tutorial. If you have questions on installing Docker or on testing the Docker environment, please ask the lecturers before the tutorial. Due to time constraints we cannot assist everyone with setting up Docker during the tutorial itself.

## Download

The Docker container can be pulled directly from [dockerhub](https://hub.docker.com/). An example command line would be

```
  docker pull cteqschool/tutorial:tasi24
```

## Running the container

We recommend to run the container for the MC tutorials with your home directory mounted and using your actual userid. In order to simplify the docker call on Linux PCs and Macs you can define an alias as follows

```
  alias docker-run='docker run -it -u $(id -u $USER) -v $HOME:$HOME -w $PWD cteqschool/tutorial:tasi24'
```
If you encounter errors with the image on an M1-based Mac, try this instead
```
  alias docker-run='docker run -it -u $(id -u $USER) -v $HOME:$HOME -w $PWD --platform=linux/amd64 cteqschool/tutorial:tasi24'
```
You may consider adding the --rm flag to automatically clean up after your container exits. Note that in this case all modifications you may have made to the container will be lost.

On Windows PCs you will need to use
```
docker run -it cteqschool/tutorial:tasi24
```
and clone or download the gitlab repository into the container itself.

## Running the tutorials

Instructions for the tutorials are found in this repository. There are three main directories, me/, ll/ and ps/, corresponding to a matrix-element, resummation and parton-shower tutorial. Please refer to the worksheets in the individual folders.
